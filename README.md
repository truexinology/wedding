# 婚礼邀请函

> 体验二维码
婚礼小程序转化版：

![小程序优化后效果](https://images.gitee.com/uploads/images/2019/1106/161213_885f9c67_1937666.png "pic_010.png")


> 如果觉得该小程序对你有所帮助，请不要吝啬你的star

> 源码之所以没有更新有以下两点原因：（一）、当初公开源码就是为了教大家实现属于自己的婚礼邀请函，更新后的界面也不符合邀请函功能，所以暂不更新；（二）、开放源码后发现很多文章存在抄袭行为，以及拿源码作为商业用途，因此，为了杜绝新功能的抄袭，暂没有打算开放更新后的源码。

> 对于想实现优化更新后的功能的朋友，不要担心，加我微信（huangbin910419）备注（‘掘金’或者‘码云’）进群，我会毫无保留的在群里教大家实现方法，群里的小伙伴或许也能帮助到你

## 以下内容为邀请函实现的文章以及注意事项，请认真查阅：

### 大家不要直接跑本项目，需要开通云开发，需要建立集合，上手前记得先熟悉云开发文档
### 结合掘金文章[项目讲解](https://juejin.im/post/5c341e1d6fb9a049f66c4876#heading-5)一起使用
### 禁止本源码作为商业用途

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## 最后给大家体验一下日记小程序
> 微信扫描下方二维码体验：

![日记小程序](https://images.gitee.com/uploads/images/2019/1106/161339_19293632_1937666.jpeg "1570776962(1).jpg")


imgs:
cloud://dev-79d5f3.2787-dev-79d5f3-1255596177/images/eb9cf8eadab678b0eeb4fa28c41e3f7a.jpg

```shell script
nvm ls-remote：列出所有可以安装的node版本号
nvm install v10.4.0：安装指定版本号的node
nvm use v10.3.0：切换node的版本，这个是全局的
nvm current：当前node版本
nvm ls：列出所有已经安装的node版本
```

```shell script
// curl -d '{ "content":"今日犹存免费成人网站，题壁有诗皆抱恨" }' 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=38_ZNvZx5qWDANWVukK7tjrOFmfarK3TGWmDUjYHM0IRKjj5G0pxChe_9ldcX68wFxnJmt7Vai7tHhWc6Si-zKl-jhEeTqpkBa17gzq1uHphfAd6gyq72y9xY4Mz8_4ga-YRN05LYfFP1bOoqaVFCZcACAKHE'
// {"errcode":87014,"errmsg":"risky content rid: 5f89b3f5-2640216a-4cefd3d4"}%

// curl -d '{ "content":"hello world!" }' 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token=38_ZNvZx5qWDANWVukK7tjrOFmfarK3TGWmDUjYHM0IRKjj5G0pxChe_9ldcX68wFxnJmt7Vai7tHhWc6Si-zKl-jhEeTqpkBa17gzq1uHphfAd6gyq72y9xY4Mz8_4ga-YRN05LYfFP1bOoqaVFCZcACAKHE'
// {"errcode":0,"errmsg":"ok"}%
```

注意事项：
小程序直接打开整个项目，而不是直接打开dist哦，别自作聪明哦。
才能点击同步云函数列表的。

## 问题总结
1.为啥要开启云端调试才能执行检测文字和图片接口正确返回呢？
// 调用 本地 云函数 'msgSecCheck' 完成  （耗时 361ms）
