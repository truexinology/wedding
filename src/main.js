import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
App.mpType = 'app'

wx.cloud.init({
  env: 'dev-79d5f3'
})

const app = new Vue(App)
app.$mount()
