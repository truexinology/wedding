// 新建个云函数文件, 例如我将其命名为 msgSecCheck
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    const { text, img } = event
    // 检查 文字内容是否违规
    if (text) {
      const res = await cloud.openapi.security.msgSecCheck({
        content: event.text
      })
      return res
    }
    // 检查 图片内容是否违规
    const res = await cloud.openapi.security.imgSecCheck({
      media: {
        header: { 'Content-Type': 'application/octet-stream' },
        contentType: 'image/png',
        value: Buffer.from(img)
      }
    })
    return res
  } catch (err) {
    return err
  }
}
